/**
 * zizi-card has a delete button with same html id as the zizi-card itself,
 * so clicking btn-delete class button identifies the zizi-card
 * @param {*} city 
 * @param {*} date 
 * @param {*} weatherData 
 */
export const addCard = (city, date, weatherData) => {
    const container = document.getElementById('cards-container');
    const weatherState = weatherData[0].weather_state_abbr;
    const minTemp = Math.round(weatherData[0].max_temp);
    const maxTemp = Math.round(weatherData[0].min_temp);
    const cardId = `${city}-${date}+${Math.floor(Math.random() * 21)}`;
    container.insertAdjacentHTML('afterbegin', `
        <zizi-card id="${cardId}" title="${city} - ${date}">
            <div class="card-content">
                <div>${minTemp}°C</div>
                <div>
                    <img height="200" src=https://www.metaweather.com/static/img/weather/${weatherState}.svg>
                </div>
                <div>${maxTemp}°C</div>      
            </div>
            <button id="${cardId}" class="btn-delete">Törlés</button>  
        </zizi-card>
    `)
}

/**
 * removes zizi-card by id (use case: delete button on the zizi-card clicked)
 * @param {*} cardId zizi-card id and button id are the same
 */
export const removeCard = (cardId) => {
    const ziziCard = document.getElementById(cardId);
    console.log('cards > removeCard: ' + cardId);
    ziziCard.remove();
}