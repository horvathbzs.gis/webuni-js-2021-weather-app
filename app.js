import { initForm } from './form';
import { removeCard } from './cards';
import './styles.css';
import 'zizi-card';

window.addEventListener('DOMContentLoaded', () => {
    initForm();
});